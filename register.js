import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Register extends Component {

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.tete}>
                    <Text style={styles.first}>Modifier/Ajouter</Text>
                    <View style={styles.second }>
                      <Icon name="times" size={30} color="#FFF" />
                    </View>
                </View>
                <View style={styles.body}>
                  <View style={{height: 200}}>
                    <Icon name="user-circle-o" size={80} color="#000" />
                  </View>

                  <TextInput
                      ref="9"
                      style={styles.input} 
                      placeholder="Numéro de Téléphone"
                      keyboardType="numeric"
                  />
                      
                  <TextInput style={styles.input} placeholder="Nom Employe" />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    //flexDirection: 'row', pour aligner horizontalement
    flex: 1,
    //justifyContent: 'flex-start',
    //alignItems: 'flex-start',
    backgroundColor: '#007db5',
  },
  tete: {
    //flex: 1,
    height: 200,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'baseline',
    width: 1000,
    marginTop: 10
  },
  first: {
    flex: 1,
    fontSize: 20,
    marginLeft: 30,
    alignSelf: 'flex-start'
  },
  second: {
    flex: 1,
  },
  body: {
    //flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    color: '#F5FCFF',
    borderBottomWidth: 2,
    borderColor: '#F5FCFF',
    width: 500,
    alignSelf: 'center',
    marginBottom: 20
  }
});